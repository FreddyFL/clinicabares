<div class="modal fade" id="modal-legal-data" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel" style="color:#000 !important;">Datos Legales</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" style="font-size: 10px; opacity: 0.2;  font-weight: bold; line-height: 1;"></button>
    {{--    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" style="font-size: 10px; opacity: 0.2;  font-weight: bold; line-height: 1;"></button>--}}
      </div>
      <div class="modal-body" style="color:#000 !important;">
        <p style="margin: 0 0 10px;font-size: 14px;
    line-height: 1.42857;
    color: #333333;">En cumplimiento de la Ley de Servicios de la Sociedad de la Información y de Comercio Electrónico (Ley 34/2002 - LSSI-CE), en vigor desde el 12 de Octubre de 2002, se pone en conocimiento público la siguiente información:</p>
        <p style="font-size: 14px;
    line-height: 1.42857;
    color: #333333;"><strong>DATOS DE CONTACTO</strong></p>
        <ul style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif;font-size: 14px; line-height: 1.42857;     color: #333333;    padding-left: 2rem; list-style: disc; display:revert !important;">
          <li style="display:revert !important;">Nombre Fiscal:{{ env('TB_REPRESENTANTE') }}</li>
          <li style="display:revert !important;"> Dirección:{{ env('TB_DOMICILIO_SOCIAL') }} </li>
          <li style="display:revert !important;">Código postal:{{ env('TB_CP') }}</li>
          <li style="display:revert !important;">Teléfono:(+34) 977 55 22 58</li>
          <li style="display:revert !important;">Email:{{ env('TB_TELEFONO') }}</li>
          <li style="display:revert !important;">NIF:{{ env('TB_NIF') }}</li>
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
