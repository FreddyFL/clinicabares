 <!-- testimonials-->
      {{--  <section class="section-98 section-sm-110 bg-default-liac">
          <div class="container">
            <h3>Testimonials</h3>
            <hr class="divider divider-lg bg-java">
            <div class="offset-top-66">
              <div class="owl-carousel owl-carousel-default owl-carousel-class-light" data-loop="false" data-items="1" data-md-items="2" data-dots="true" data-mouse-drag="false" data-xl-items="3" data-nav="false">
                <blockquote class="quote quote-classic">
                  <div class="quote-body">
                    <p class="font-italic text-gray-dark">
                      <q>I can't recommend this dental practice highly enough, visited recently as a new patient requiring treatment. I found the reception staff to be most welcoming, approachable, and professional. </q>
                    </p>
                    <div class="quote-meta unit flex-row unit-spacing-sm align-items-center">
                      <div class="unit-left unit-item-narrow"><img class="rounded-circle quote-img" width="60" height="60" src="images/users/user-betty-wade-60x60.jpg" alt=""/></div>
                      <div class="unit-body unit-item-wide">
                        <h5 class="quote-author text-capitalize text-primary">
                          <cite class="text-normal">Betty Wade</cite>
                        </h5>
                        <p class="quote-desc text-capitalize text-gray font-italic">Regular Regular Patient</p>
                      </div>
                    </div>
                  </div>
                </blockquote>
                <blockquote class="quote quote-classic">
                  <div class="quote-body">
                    <p class="font-italic text-gray-dark">
                      <q>Our family of five have been with this practice for 12 years.  All three children were referred for orthodontic work and I have had a few issues, which were always dealt with efficiently.</q>
                    </p>
                    <div class="quote-meta unit flex-row unit-spacing-sm align-items-center">
                      <div class="unit-left unit-item-narrow"><img class="rounded-circle quote-img" width="60" height="60" src="images/users/user-bryan-green-60x60.jpg" alt=""/></div>
                      <div class="unit-body unit-item-wide">
                        <h5 class="quote-author text-capitalize text-primary">
                          <cite class="text-normal">Bryan Green</cite>
                        </h5>
                        <p class="quote-desc text-capitalize text-gray font-italic">Regular Patient</p>
                      </div>
                    </div>
                  </div>
                </blockquote>
                <blockquote class="quote quote-classic">
                  <div class="quote-body">
                    <p class="font-italic text-gray-dark">
                      <q>After 55 years of using dentists, I’ve finally found those that just work. Appointment system is brilliant, always very accommodating with times, which is perfect for me.</q>
                    </p>
                    <div class="quote-meta unit flex-row unit-spacing-sm align-items-center">
                      <div class="unit-left unit-item-narrow"><img class="rounded-circle quote-img" width="60" height="60" src="images/users/user-joan-anderson-60x60.jpg" alt=""/></div>
                      <div class="unit-body unit-item-wide">
                        <h5 class="quote-author text-capitalize text-primary">
                          <cite class="text-normal">Joan Anderson</cite>
                        </h5>
                        <p class="quote-desc text-capitalize text-gray font-italic">Regular Patient</p>
                      </div>
                    </div>
                  </div>
                </blockquote>
                <blockquote class="quote quote-classic">
                  <div class="quote-body">
                    <p class="font-italic text-gray-dark">
                      <q>I can't recommend this dental practice highly enough, visited recently as a new patient requiring treatment. I found the reception staff to be most welcoming, approachable, and professional. </q>
                    </p>
                    <div class="quote-meta unit flex-row unit-spacing-sm align-items-center">
                      <div class="unit-left unit-item-narrow"><img class="rounded-circle quote-img" width="60" height="60" src="images/users/user-betty-wade-60x60.jpg" alt=""/></div>
                      <div class="unit-body unit-item-wide">
                        <h5 class="quote-author text-capitalize text-primary">
                          <cite class="text-normal">Betty Wade</cite>
                        </h5>
                        <p class="quote-desc text-capitalize text-gray font-italic">Regular Regular Patient</p>
                      </div>
                    </div>
                  </div>
                </blockquote>
                <blockquote class="quote quote-classic">
                  <div class="quote-body">
                    <p class="font-italic text-gray-dark">
                      <q>Our family of five have been with this practice for 12 years.  All three children were referred for orthodontic work and I have had a few issues, which were always dealt with efficiently.</q>
                    </p>
                    <div class="quote-meta unit flex-row unit-spacing-sm align-items-center">
                      <div class="unit-left unit-item-narrow"><img class="rounded-circle quote-img" width="60" height="60" src="images/users/user-bryan-green-60x60.jpg" alt=""/></div>
                      <div class="unit-body unit-item-wide">
                        <h5 class="quote-author text-capitalize text-primary">
                          <cite class="text-normal">Bryan Green</cite>
                        </h5>
                        <p class="quote-desc text-capitalize text-gray font-italic">Regular Patient</p>
                      </div>
                    </div>
                  </div>
                </blockquote>
                <blockquote class="quote quote-classic">
                  <div class="quote-body">
                    <p class="font-italic text-gray-dark">
                      <q>After 55 years of using dentists, I’ve finally found those that just work. Appointment system is brilliant, always very accommodating with times, which is perfect for me.</q>
                    </p>
                    <div class="quote-meta unit flex-row unit-spacing-sm align-items-center">
                      <div class="unit-left unit-item-narrow"><img class="rounded-circle quote-img" width="60" height="60" src="images/users/user-joan-anderson-60x60.jpg" alt=""/></div>
                      <div class="unit-body unit-item-wide">
                        <h5 class="quote-author text-capitalize text-primary">
                          <cite class="text-normal">Joan Anderson</cite>
                        </h5>
                        <p class="quote-desc text-capitalize text-gray font-italic">Regular Patient</p>
                      </div>
                    </div>
                  </div>
                </blockquote>
              </div>
            </div>
          </div>
          <!-- RD Parallax-->
        </section>--}}