 {{-- <section class="section-two-column parallax-container" data-parallax-img="images/parallax-1.jpg">
        <div class="parallax-content">
          <div class="google-map-container" data-center="9870 St Vincent Place, Glasgow, DC 45 Fr 45." data-styles="[{&quot;featureType&quot;:&quot;landscape&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:-100},{&quot;lightness&quot;:60}]},{&quot;featureType&quot;:&quot;road.local&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:-100},{&quot;lightness&quot;:40},{&quot;visibility&quot;:&quot;on&quot;}]},{&quot;featureType&quot;:&quot;transit&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:-100},{&quot;visibility&quot;:&quot;simplified&quot;}]},{&quot;featureType&quot;:&quot;administrative.province&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;on&quot;},{&quot;lightness&quot;:30}]},{&quot;featureType&quot;:&quot;road.highway&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#ef8c25&quot;},{&quot;lightness&quot;:40}]},{&quot;featureType&quot;:&quot;road.highway&quot;,&quot;elementType&quot;:&quot;geometry.stroke&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;poi.park&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#b6c54c&quot;},{&quot;lightness&quot;:40},{&quot;saturation&quot;:-40}]},{}]" data-zoom="14">
            <div class="google-map"></div>
            <ul class="google-map-markers">
              <li data-location="9870 St Vincent Place, Glasgow, DC 45 Fr 45." data-description="9870 St Vincent Place, Glasgow" data-icon="images/gmap_marker.png" data-icon-active="images/gmap_marker_active.png"> </li>
            </ul>
          </div>
          <div class="container context-dark text-lg-left">
            <div class="row justify-content-lg-end justify-content-center">
              <div class="col-sm-10 col-lg-5">
                <div class="section-85">
                  <h6>contact us</h6>
                  <hr>
                  <div class="text-center text-lg-left">
                    <address class="contact-info d-md-inline-block text-left">
                      <div class="p unit unit-spacing-xxs flex-row">
                        <div class="unit-left"><span class="icon icon-xxs mdi mdi-phone"></span></div>
                        <div class="unit-body"><a href="tel:#">1-800-1234-567</a><span class="text-gray-light">, </span><a href="tel:#">1-800-3214-321</a></div>
                      </div>
                      <div class="p unit flex-row unit-spacing-xxs">
                        <div class="unit-left"><span class="icon icon-xxs mdi mdi-map-marker"></span></div>
                        <div class="unit-body"><a href="#">2130 Fulton Street San Diego, CA 94117-1080 USA</a></div>
                      </div>
                      <div class="p unit unit-spacing-xxs flex-row offset-top-16">
                        <div class="unit-left"><span class="icon icon-xxs mdi mdi-email-outline"></span></div>
                        <div class="unit-body"><a href="mailto:#">info@demolink.org</a></div>
                      </div>
                    </address>
                  </div>
                  <div class="offset-top-50">
                    <h6>Newsletter</h6>
                    <hr>
                    <p>Enter your email address to receive up-to-date news, new patient information and other useful stuff, delivered right to your inbox.</p>
                          <form class="rd-mailform" data-form-output="form-output-global" data-form-type="subscribe" method="post" action="bat/rd-mailform.php">
                            <div class="form-wrap">
                              <div class="input-group input-group-sm">
                                <input class="form-input text-gray-darker" placeholder="Your e-mail..." type="email" data-constraints="@Required @Email" name="email"><span class="input-group-btn">
                                  <button class="btn btn-sm btn-java" type="submit">Subscribe</button></span>
                              </div>
                            </div>
                          </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>--}}