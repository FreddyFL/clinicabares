<footer class="section-relative section-34 section-md-50 page-footer bg-default footer-font">
	<div class="container">
	  <div class="inset-xxl-left-30">
	    <div class="row justify-content-md-center">
	      <div class="col-md-8 col-lg-12">
	        <div class="row justify-content-lg-between align-items-lg-center row-50">
	          <div class="col-lg-2 col-xl-2 order-lg-1 text-lg-left">
	            <!-- Footer brand-->
	            <div class="footer-brand d-inline-block"><a href="index.html"><img width='500' height='52' class='img-responsive' src='images/logo-dark.png' alt=''/></a></div>
	          </div>
	        <div class="col-lg-4 col-xl-3 order-lg-3 text-lg-right">
	                {{--    <ul class="list-inline">
	                    <li class="list-inline-item"><a class="icon fa fa-facebook icon-xxs icon-circle icon-gray-light" href="#"></a></li>
	                    <li class="list-inline-item"><a class="icon fa fa-twitter icon-xxs icon-circle icon-gray-light" href="#"></a></li>
	                    <li class="list-inline-item"><a class="icon fa fa-google-plus icon-xxs icon-circle icon-gray-light" href="#"></a></li>
	                    <li class="list-inline-item"><a class="icon fa fa-rss icon-xxs icon-circle icon-gray-light" href="#"></a></li>
	                  </ul>--}}
	          </div>
	          <div class="col-lg-5 col-xl-7 order-lg-2">
	            <p class="mb-0">&copy; <span class="copyright-year"></span> TecnoBravo &nbsp; | &nbsp;
	<a  href="#" data-bs-toggle="modal" data-bs-target="#modal-legal-notice">Aviso Legal</a>
	&nbsp; | &nbsp; 
	<a href="#" data-bs-toggle="modal" data-bs-target="#modal-legal-data">Datos Legales</a></span>
	            </p>
	             <p> <br></p>
	                 <p> <br></p>
	           <p><small>Web diseñada y albergada por <a href="https://tecnobravo.com" target="_blank"><strong>TecnoBravo IT</strong></a></small>  </p>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
</footer>

