<div class="rd-navbar-wrap" style="height:auto !important">
          <nav class="rd-navbar rd-navbar-minimal rd-navbar-light" data-layout="rd-navbar-fixed" data-sm-device-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fixed" data-xl-device-layout="rd-navbar-static" data-xl-layout="rd-navbar-static" data-lg-auto-height="true" data-lg-stick-up="true">
            <div class="rd-navbar-inner" style="padding: 10px 15px; top:0 !important;">
            	<div class="d-flex flex-row justify-content-between">
					<div  class="d-flex flex-row align-items-center">
						 <div class="rd-navbar-brand d-none d-lg-inline-block"><a href="index.html"><img class='img-responsive' src='images/logo.png' style="margin-top: -8px; width: 83px;" alt=''/></a></div>
						 <!-- RD Navbar Panel-->
              <div class="rd-navbar-panel">
                <!-- RD Navbar Toggle-->
                <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar, .rd-navbar-nav-wrap"><span></span></button>
                <!--Navbar Brand-->
                <div class="rd-navbar-brand d-xl-none"><a href="index.html"><img  class='img-responsive' src='images/logo.png' style="margin-top: -8px; width: 83px; " alt=''/></a></div>
                <button class="rd-navbar-top-panel-toggle" data-rd-navbar-toggle=".rd-navbar, .rd-navbar-top-panel"><span></span></button>
              </div>
              <div class="rd-navbar-menu-wrap">
                <div class="rd-navbar-nav-wrap">
                  <div class="rd-navbar-mobile-scroll">
                    <!--Navbar Brand Mobile-->
                    <div class="rd-navbar-mobile-brand"><a href="index.html"><img width='179' height='52' class='img-responsive' src='images/logo.png' alt=''/></a></div>
                    <div class="form-search-wrap">
                      <!-- RD Search Form-->
                      <form class="form-search rd-search" action="search-results.html" method="GET">
                        <div class="form-wrap">
                          <label class="form-label form-search-label form-label-sm" for="rd-navbar-form-search-widget">Search</label>
                          <input class="form-search-input form-input form-input-gray-lightest input-sm" id="rd-navbar-form-search-widget" type="text" name="s" autocomplete="off"/>
                        </div>
                        <button class="form-search-submit" type="submit"><span class="fa fa-search text-primary"></span></button>
                      </form>
                    </div>
                    <!-- RD Navbar Nav-->
                    <ul class="rd-navbar-nav">
                      <li class="active"><a href="#inicio"><span>Inicio</span></a>
                      </li>
                      <li><a href="#contacto"><span>Contacto</span></a>
                      </li>
                         <li class="list-inline-item"><a class="fa fa-instagram icono-tam" href="https://www.instagram.com/clinicabares/" target="_blank"></a></li>
                       <li class="list-inline-item"><a class="fa fa-facebook icono-tam" href="https://www.facebook.com/ClinicaBares/" target="_blank"></a></li>
                   
                      
                    </ul>
                  </div>
                </div>
                <!--RD Navbar Search-->
                {{--<div class="rd-navbar-search"><a class="rd-navbar-search-toggle mdi" data-rd-navbar-toggle=".rd-navbar-menu-wrap,.rd-navbar-search" href="#"><span></span></a>
                  <form class="rd-navbar-search-form search-form-icon-right rd-search" action="search-results.html" data-search-live="rd-search-results-live" method="GET">
                    <div class="form-wrap">
                      <label class="form-label" for="rd-navbar-search-form-input">Type and hit enter...</label>
                      <input class="rd-navbar-search-form-input form-input form-input-gray-lightest" id="rd-navbar-search-form-input" type="text" name="s" autocomplete="off"/>
                    </div>
                    <div class="rd-search-results-live" id="rd-search-results-live"></div>
                  </form>
                </div>--}}
              </div>
					</div>
					<div class="rd-navbar-top-panel d-flex flex-column ">
						 
						<div >
							<address class="contact-info d-md-inline-block text-left offset-top-10 offset-md-top-0">
					          <div class="p unit unit-spacing-xs flex-row">
					            <div class="unit-left">
					            	<span class="fa fa-clock-o mdi mdi-phone" style="color: #029948;"></span>
					            </div>
					            <div class="unit-body">
					            	<a class="text-gray-darker" href="#" style="text-decoration: none;">Lun-Vie: 09 a 14<br/>Lun y Mie: 15 a 17</a>
					            </div>
					          </div>
					        </address>
					    </div>
						<div >
							<address class="contact-info d-md-inline-block text-left offset-top-10 offset-md-top-0">
								<div class="p unit unit-spacing-xs flex-row">
									<div class="unit-left">
										<span class="fab fa-whatsapp  mdi mdi-phone" style="color: #029948;"></span>
									</div>
									<div class="unit-body">
                    <a class="text-gray-darker" href="intent://send/+34628015890#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end" style="text-decoration: none;">628 01 58 90</a>
									</div>
								</div>
							</address>
						</div>
						<div  >
							<address class="contact-info d-md-inline-block text-left offset-top-10 offset-md-top-0">
    						<div class="p unit unit-spacing-xs flex-row">
      						<div class="unit-left">
      							<span class="gi gi-earphone  mdi mdi-phone" style="color: #029948;"></span>
      						</div>
      						<div class="unit-body">
                    <a class="text-gray-darker" href="tel:+34977590159" style="text-decoration: none;">977 59 01 59</a>
      						</div>
		           </div>
					   </address>
						</div>
					</div>
				</div>
      </div>
    </nav>
</div>