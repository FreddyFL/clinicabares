$(function(){
	const token="IGQVJYOXFaV3hHMndOQlB1WWI1cUoyRzFUbjRuZAkh4SDB2TzM1dmh6RHdqalRtVFBLR083ek5rY1AwZAThZAS3pLa0FZATS1LSHdSQlhVVWdzalZALeGhKb0NXQ1FUZAzhxMC1vQ2Q2UlpXQ1VsUVBrRDdWVgZDZD";
	const url="https://graph.instagram.com/me/media?access_token="+token+"&fields=media_url,media_type,caption,permalink";
	$.get(url).then(function(response){
		let datosJson=response.data;
		let contenido='';
		for(let p=0; p<9; p++){
			let feed=datosJson[p];
			let titulo=feed.caption !== null ? feed.caption :'';
			let tipo= feed.media_type;
			if(tipo=== 'VIDEO'){
				contenido += '<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4 col-xxl-4"><video style="width:100%; height:90%;" controls><source src="'+feed.media_url+'" type="video/mp4"></video></div>';				
			}
			if(tipo ==='CAROUSEL_ALBUM'){
		    	contenido += '<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4 col-xxl-4"><img style="width:100%;height:90%;" title="'+titulo+'" alt="'+titulo+'" src="'+feed.media_url+'" onclick="window.open(\''+feed.permalink+'\');"></div>';				
			}
			else if (tipo==='IMAGE'){
				contenido += '<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4 col-xxl-4"><img style="width:100%;height:90%;" title="'+titulo+'" alt="'+titulo+'" src="'+feed.media_url+'" onclick="window.open(\''+feed.permalink+'\');"></div>';				
			}
			contenido += "</div>";
			$("#instagram").html(contenido);
		}
	});
});