<br>
<br>
 <section>
  <div class="row no-gutters">
    <div class="col-sm-6 col-md-4 col-xl-2">
      <!-- Thumbnail Josip-->
      <figure class="thumbnail-josip service-modern  "><a href="#"><img width="384" height="410" src="images/service-1-320x320.jpg" alt=""/></a>
        <div class="thumbnail-desc">
          <h6 class="thumbnail-josip-title font-weight-bold"><a class="text-white" href="services.html">Dentures</a></h6>
        </div>
        <figcaption><a class="btn btn-block btn-rect text-lg-left btn-java" href="make-an-appointment.html">book an appointment</a></figcaption>
      </figure>
    </div>
    <div class="col-sm-6 col-md-4 col-xl-2">
      <!-- Thumbnail Josip-->
      <figure class="thumbnail-josip service-modern  odd"><a href="#"><img width="384" height="410" src="images/service-2-320x320.jpg" alt=""/></a>
        <div class="thumbnail-desc">
          <h6 class="thumbnail-josip-title font-weight-bold"><a class="text-white" href="services.html">Cosmetic Dentistry</a></h6>
        </div>
        <figcaption><a class="btn btn-block btn-rect text-lg-left btn-java" href="make-an-appointment.html">book an appointment</a></figcaption>
      </figure>
    </div>
    <div class="col-sm-6 col-md-4 col-xl-2">
      <!-- Thumbnail Josip-->
      <figure class="thumbnail-josip service-modern mt-lg-0"><a href="#"><img width="384" height="410" src="images/service-3-320x320.jpg" alt=""/></a>
        <div class="thumbnail-desc">
          <h6 class="thumbnail-josip-title font-weight-bold"><a class="text-white" href="services.html">Dental Cleanings</a></h6>
        </div>
        <figcaption><a class="btn btn-block btn-rect text-lg-left btn-java" href="make-an-appointment.html">book an appointment</a></figcaption>
      </figure>
    </div>
    <div class="col-sm-6 col-md-4 col-xl-2">
      <!-- Thumbnail Josip-->
      <figure class="thumbnail-josip service-modern  mt-lg-0 odd"><a href="#"><img width="384" height="410" src="images/service-4-320x320.jpg" alt=""/></a>
        <div class="thumbnail-desc">
          <h6 class="thumbnail-josip-title font-weight-bold"><a class="text-white" href="services.html">Pediatric dentistry</a></h6>
        </div>
        <figcaption><a class="btn btn-block btn-rect text-lg-left btn-java" href="make-an-appointment.html">book an appointment</a></figcaption>
      </figure>
    </div>
    <div class="col-sm-6 col-md-4 col-xl-2">
      <!-- Thumbnail Josip-->
      <figure class="thumbnail-josip service-modern  mt-lg-0"><a href="#"><img width="384" height="410" src="images/service-5-320x320.jpg" alt=""/></a>
        <div class="thumbnail-desc">
          <h6 class="thumbnail-josip-title font-weight-bold"><a class="text-white" href="services.html">Dental Implants</a></h6>
        </div>
        <figcaption><a class="btn btn-block btn-rect text-lg-left btn-java" href="make-an-appointment.html">book an appointment</a></figcaption>
      </figure>
    </div>
    <div class="col-sm-6 col-md-4 col-xl-2">
      <!-- Thumbnail Josip-->
      <figure class="thumbnail-josip service-modern  mt-lg-0 odd"><a href="#"><img width="384" height="410" src="images/service-6-320x320.jpg" alt=""/></a>
        <div class="thumbnail-desc">
          <h6 class="thumbnail-josip-title font-weight-bold"><a class="text-white" href="services.html">Orthodontics</a></h6>
        </div>
        <figcaption><a class="btn btn-block btn-rect text-lg-left btn-java" href="make-an-appointment.html">book an appointment</a></figcaption>
      </figure>
    </div>
  </div>
</section>