   <!-- latest blog posts-->
     {{--    <section class="section-98 section-sm-110">
          <div class="container">
            <h3>latest blog posts</h3>
            <hr class="divider divider-lg bg-java">
            <div class="row justify-content-sm-center row-50 offset-top-66">
              <div class="col-md-6">
                <!-- Post Modern-->
                <article class="post post-modern post-modern-classic">
                  <!-- Post media-->
                  <header class="post-media"><a href="single-post.html"><img width="570" height="355" src="images/blog/post-08-770x480.jpg" alt=""/></a>
                  </header>
                  <!-- Post content-->
                  <section class="post-content text-left">
                    <!-- Post Title-->
                    <div class="post-title offset-top-8">
                      <h4><a href="single-post.html">First Signs of Gum Disease</a></h4>
                    </div>
                    <ul class="list-inline list-inline-dashed">
                      <li class="list-inline-item">June 21, 2018 at 8:12pm</li>
                      <li class="list-inline-item"><a class="text-java" href="blog-masonry.html">News</a></li>
                    </ul>
                    <!-- Post Body-->
                    <div class="post-body">
                      <p>Gum disease is preceded by few very distinct symptoms. One of these is when you spit out blood after brushing your teeth...</p>
                    </div>
                    <div class="tags group group-sm"><a class="btn-tag btn btn-default" href="#">News</a><a class="btn-tag btn btn-default" href="#">Tips</a><a class="btn-tag btn btn-default" href="#">Dental</a>
                    </div>
                  </section>
                </article>
              </div>
              <div class="col-md-6">
                <!-- Post Modern-->
                <article class="post post-modern post-modern-classic">
                  <!-- Post media-->
                  <header class="post-media"><a href="single-post.html"><img width="570" height="355" src="images/blog/post-09-770x480.jpg" alt=""/></a>
                  </header>
                  <!-- Post content-->
                  <section class="post-content text-left">
                    <!-- Post Title-->
                    <div class="post-title offset-top-8">
                      <h4><a href="single-post.html">Basic dental care 101</a></h4>
                    </div>
                    <ul class="list-inline list-inline-dashed">
                      <li class="list-inline-item">June 21, 2018 at 8:12pm</li>
                      <li class="list-inline-item"><a class="text-java" href="blog-masonry.html">News</a></li>
                    </ul>
                    <!-- Post Body-->
                    <div class="post-body">
                      <p>Americans oftentimes ignore some basic, daily routines of oral hygiene that need to be upheld. Practicing healthy habits like these ones will...</p>
                    </div>
                    <div class="tags group group-sm"><a class="btn-tag btn btn-default" href="#">News</a><a class="btn-tag btn btn-default" href="#">Tips</a><a class="btn-tag btn btn-default" href="#">Dental</a>
                    </div>
                  </section>
                </article>
              </div>
            </div>
            <div class="offset-top-50"><a class="btn btn-ellipse btn-java" href="blog-grid.html">View all blog posts</a></div>
          </div>
        </section>--}}