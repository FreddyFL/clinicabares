<!--Swiper para que el slide pueda desplazarse de manera automatica se coloca la variable data-autoplay igual a true en el div donde esta el id inicio-->
        <div id="inicio" class="swiper-container swiper-slider bg-default"  data-autoplay="true" data-loop="true" data-height="" data-slide-effect="fade" data-dragable="false" data-min-height="400px">
          <div class="swiper-wrapper text-center context-dark ">
            <div class="swiper-slide img-slide img-slide" data-slide-bg="images/swiper-slide-3.jpg"  >
               <div class="swiper-caption" style="background-color: #494949; ">
                <div class="swiper-slide-caption">
                  <div class="container">
                    <div class="row justify-content-center">
                      <div class="col-lg-12 col-xl-12 section-41 section-sm-98 section-md-124 d-flex justify-content-center align-items-center" >
                        <div class="text-center text-sm-center text-md-center text-lg-center">
                         {{-- <h2 class="offset-top-20">CLINICA BARES</h2>--}}
                         <img src="{{asset('images/clinicabares.png')}}" class="offset-top-20 img-clinica">
                         <br>
                         <a class="btn btn-ellipses btn-outline-white offset-top-24" href="#contacto">CONTACTO</a>
                          <br>
                          {{--<a href="#contacto" style="color:#fff "><img src="{{asset('images/flechablanca.png')}}"></a>--}}
                          <br>
                          <br>
                          
                          <a href="#contacto" style="color:#fff "><img src="{{asset('images/flechablanca.png')}}" style="width:50px;"></a>        
                        </div>
                      </div>
                    </div>
                </div>
                </div>
              </div>
            </div>
            <div class="swiper-slide img-slide img-slide" data-slide-bg="images/swiper-slide-1.jpg"  >
              <div class="swiper-caption">
                <div class="swiper-slide-caption">
                  <div class="container">
                    <div class="row justify-content-center">
                       <div class="col-lg-12 col-xl-12 section-41 section-sm-98 section-md-124 d-flex justify-content-center align-items-center">
                        <div class="text-center text-sm-center text-md-center text-lg-center">
                          <img src="{{asset('images/clinicabares.png')}}" class="offset-top-20 img-clinica">
                      {{--    <h2 class="offset-top-20">CLINICA BARES</h2>--}}
                      <br>
                      <a class="btn btn-ellipses btn-outline-white offset-top-24" href="#contacto">CONTACTO</a>
                          <br>
                          <br>
                          <br>
                          <a href="#contacto" style="color:#fff "><img src="{{asset('images/flechablanca.png')}}" style="width:50px;"></a>        
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="swiper-slide img-slide" data-slide-bg="images/swiper-slide-2.jpg" style="width: 400px;">
              <div class="swiper-caption">
                <div class="swiper-slide-caption">
                  <div class="container">
                    <div class="row justify-content-center">
                      <div class="col-lg-12 col-xl-12 section-41 section-sm-98 section-md-124 d-flex justify-content-center align-items-center">
                        <div class="text-center text-sm-center text-md-center text-lg-center">
                              <img src="{{asset('images/clinicabares.png')}}" class="offset-top-20 img-clinica" >
                      {{--    <h2 class="offset-top-20">CLINICA BARES</h2>--}}
                      <br>
                      <a class="btn btn-ellipses btn-outline-white offset-top-24" href="#contacto">CONTACTO</a>
                          <br>
                          <br>
                          <br>
                          <a href="#contacto" style="color:#fff "><img src="{{asset('images/flechablanca.png')}}" style="width:50px;"></a>        
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
                       
          <div class="swiper-pagination">
          
          </div>
        </div>