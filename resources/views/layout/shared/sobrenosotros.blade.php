   <section id="servicios">
          <div class="container">
            
            <div class="row justify-content-sm-center justify-content-md-end offset-top-0">
              <div class="col-sm-10 col-md-6 section-image-aside section-image-aside-left text-center text-md-left">
                <div class="section-image-aside-img d-none d-md-block" style="background-image: url('images/backgrounds/background-02-676x5455.jpg')"></div>
                <div class="section-image-aside-body offset-top-66 offset-sm-top-0 section-sm-bottom-66 section-sm-top-110 inset-lg-left-93">
                  <h3 style="color:#029948 !important;">SOBRE NOSOTROS</h3>
                  <hr class="divider divider-lg hr-sm-left-2 bg-java">
                  <div class="offset-top-34 offset-md-top-60">
                  	<p>Somos una clínica dental abierta en 1999 para ofrecer un tratamiento multidisciplinario con la máxima tecnología, sin tener que cambiar de centro ni de profesional para cualquier tratamiento.</p>
					<p>Disponemos de rx panorámica digital, rx 3D (CBCT), Láser, toma de color con espectrofotografía , CAD_CAM, cirugía mínimamente invasiva, endodoncia mecánica con localizador de ápices,ortodoncia lingual, etc.</p>
                    <p>Colaboramos con un técnico de laboratorio excepcional con gran experiencia, lo que nos da un plus a la hora de dar calidad a nuestros trabajos.</p>
                  </div>
               {{--   <div class="offset-top-30"><a class="btn btn-ellipse btn-java" href="#">Learn more</a></div> --}}
                </div>
              </div>
            </div>
          
          </div>
        </section>