<section class="section-98" id="contacto">
<div class="container">
  <div class="row justify-content-sm-center">
    <div class="col-md-9 col-lg-8 col-xl-12">
      <div class="row">
        <div class="col-xl-6 text-left offset-top-66 offset-xl-top-0">
          <h3 class="text-center text-sm-center text-md-center text-lg-center" style="color:#029948 !important;">INSTAGRAM</h3>
            <div class="row" id="instagram">
            </div>
        </div>
        <div class="col-xl-6 text-left">
          <h3 style="color:#029948 !important; text-align: center;">CONTACTO</h3>
          <hr class="divider divider-lg bg-java hr-left-0hr-sm-left-2">
          <div class="offset-top-66">
			<address class="contact-info d-md-inline-block text-left offset-top-10 offset-md-top-0">
				<div class="p unit unit-spacing-xs flex-row">
					<div class="unit-left">
					   <span class="fa fa-clock-o mdi mdi-phone" style="color: #029948;"></span>
					</div>
					<div class="unit-body">
					   <p>Lunes a Viernes de : 09 a 14</p>
					</div>
				</div>
				<div class="p unit unit-spacing-xs flex-row">
					<div class="unit-left">
					   <span class="fa fa-clock-o mdi mdi-phone" style="color: #029948;"></span>
					</div>
					<div class="unit-body">
					   <p>Lunes y Miércoles de : 15 a 17</p>
					</div>
				</div>			
			</address>
			  <p></p>
            <p>¡No dude en ponerse en contacto con nosotros!</p>
            <p>Nuestros profesionales se pondrán en contacto con usted para darle la mejor atención.</p>
          </div>
          <div class="offset-top-34">
             <input type="hidden" id="base_url" value="{{url('/')}}">
            @if ($notify = session('notify'))
              <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <!--     <span aria-hidden="true">x</span>-->
                </button>
                <strong>Información: </strong> {{ $notify['msg'] }}
              </div>
            @endif
            <form id="estilo"  action="{{ route('contacto') }}"  class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php" novalidate="novalidate">
              <div id="loading-screen" style="display: none;"> 
                <div class="spinner-border" role="status">
                    <span class="visually-hidden">Loading...</span>
                </div>
              </div>    
              @csrf
              <div class="row justify-content-sm-center">
                <div class="col-md-6">
                  <div class="form-wrap form-wrap-label-outside">
                    <label class="form-label form-label-outside text-dark rd-input-label" for="contacts-first-name">Nombre</label>
                    <input class="form-input form-input-last-child @error('contacts-first-name') is-invalid @enderror" id="contacts-first-name" type="text" name="first-name" data-constraints="@Required"><span class="form-validation"></span>
                    <div id="nameFeedback" class="invalid-feedback pl-1"></div>
                  </div>

                  <div class="form-wrap form-wrap-label-outside offset-top-20">
                    <label class="form-label form-label-outside text-dark rd-input-label" for="contacts-email">Correo</label>
                    <input class="form-input form-input-last-child @error('contacts-email') is-invalid @enderror" id="contacts-email" type="email" name="email" data-constraints="@Email @Required"><span class="form-validation"></span>
                    <div id="emailFeedback" class="invalid-feedback pl-1"></div>
                  </div>
                </div>
                <div class="col-md-6 offset-top-20 offset-md-top-0">
                  <div class="form-wrap form-wrap-label-outside">
                    <label class="form-label form-label-outside text-dark rd-input-label" for="contacts-last-name">Apellido</label>
                    <input class="form-input form-input-last-child @error('contacts-last-name') is-invalid @enderror" id="contacts-last-name" type="text" name="last-name" data-constraints="@Required"><span class="form-validation"></span>
                     <div id="lastnameFeedback" class="invalid-feedback pl-1"></div>
                  </div>
                  <div class="form-wrap form-wrap-label-outside offset-top-20">
                    <label class="form-label form-label-outside text-dark rd-input-label" for="contacts-phone">Teléfono</label>
                    <input class="form-input form-input-last-child @error('contacts-phone') is-invalid @enderror" id="contacts-phone" type="text" name="last-name" data-constraints=" @Required @Numeric"><span class="form-validation"></span>
                    <div id="phoneFeedback" class="invalid-feedback pl-1"></div>
                  </div>
                </div>
              </div>
              <div class="form-wrap form-wrap-label-outside offset-top-20">
                <label class="form-label form-label-outside text-dark rd-input-label" for="contacts-message">Mensaje</label>
                <textarea class="form-input form-input-last-child" id="contacts-message" name="message" data-constraints="@Required" style="max-height: 150px;"></textarea><span class="form-validation"></span>
                 <div id="messageFeedback" class="invalid-feedback pl-1"></div>
              </div>
              <div class="offset-top-20 offset-sm-top-24 text-center text-md-left">
                <button class="btn btn-ellipse btn-java" type='button' onclick="formulario()" style="min-width: 130px;">ENVIAR</button>
              </div>
            </form>
          </div>
        </div>
        
      </div>
    </div>
  </div>
</div>
</section>