<!DOCTYPE html>
<html class="wide wow-animation smoothscroll scrollTo" lang="en"> 
  <head>
    <!-- Site Title-->
    <title>CLINICA BARES</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="keywords" content="SANA design multipurpose template">
    <meta name="date" content="Dec 26">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Oswald%7CLato:400italic,400,700%7CSignika:400,600">
    
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link href="{{asset('css/estilo.css')}}" rel="stylesheet">
    <link href="{{asset('css/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/fonts.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
	  <link rel="stylesheet" href="{{ asset('css/clinica-bares.css') }}">
    <link rel="stylesheet" href="{{ url('css/sweetalert2.css') }}">
     @yield('css')
  </head>
  <body>
 {{--   <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>--}}
    <div class="preloader">
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center"> 
      <!-- Page Head-->
      <header class="page-head">
        <!-- RD Navbar Transparent        -->
         @include('layout.shared.navbar')
        <!--Swiper-->
        @include('layout.shared.swiper')
        <!--Swiper end-->
      </header>
      <!-- Page Contents-->
      <main class="page-content">

        @include('layout.shared.sobrenosotros')
        @include('layout.shared.gallery')
        <!--What makes us different-->
        @include('layout.shared.section_98')
        @include('layout.shared.ourachievements')
        @include('layout.shared.newemergency')
        @include('layout.shared.ourteam') 
        @include('layout.shared.testimonials') 
       @include('layout.shared.blogposts') 
        @include('layout.shared.contactus')
        @yield('content') 
      </main>
      <!-- Page Footer-->
      @include('layout.shared.mapa')
      <!-- Default footer-->
      @include('layout.shared.footer')
    </div>
    <!-- Global Mailform Output-->
   {{-- <div class="snackbars" id="form-output-global"> </div>--}}
    <!-- Java script-->
    <script src="{{ asset('js/core.min.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>
    <script src="{{ url('js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('js/contacto.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
     @yield('javascript')
  </body>
</html>

@include('layout.shared.modal_legal_data')
@include('layout.shared.modal_legal_notice')

