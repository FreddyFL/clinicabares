 <!-- Our Achievements-->
<section class="text-md-left bg-default-liac">
  <div class="container-wide">
    <div class="row justify-content-sm-center">
      <div class="col-md-10 col-lg-12 col-xl-7 col-xxl-8 section-85">
        <div class="inset-lg-left-60 inset-xl-left-100">
          <h3 style="color:#029948 !important;">DR. MIQUEL BARÉS</h3>
          <hr class="divider divider-lg bg-java hr-sm-left-2">
          <div class="offset-top-41 offset-lg-top-60">
            <div class="row">
              <div class="col-lg-12 col-xxl-9">
                <div class="row row-50">
                  <div class="col-lg-6">
                    <!-- Icon Box Type 2-->
                    <div class="unit unit-sm flex-md-row unit-spacing-sm text-left align-items-center">
                      <div class="unit-left"><span class="icon icon icon-sm icon-circle text-java icon-default mdi mdi-trophy-variant"></span></div>
                      <div class="unit-body">
                        <h5 class="font-weight-bold font-weight-bold text-primary">The Best Dental Center 2018</h5>
                        <p></p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <!-- Icon Box Type 2-->
                    <div class="unit unit-sm flex-md-row unit-spacing-sm text-left align-items-center">
                      <div class="unit-left"><span class="icon icon icon-sm icon-circle text-java icon-default  mdi mdi-pill copy"></span></div>
                      <div class="unit-body">
                        <h5 class="font-weight-bold font-weight-bold text-primary">Certified by the International Association</h5>
                        <p></p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <!-- Icon Box Type 2-->
                    <div class="unit unit-sm flex-md-row unit-spacing-sm text-left align-items-center">
                      <div class="unit-left"><span class="icon icon icon-sm icon-circle text-java icon-default  mdi mdi-school"></span></div>
                      <div class="unit-body">
                        <h5 class="font-weight-bold font-weight-bold text-primary">The Best Doctors Award</h5>
                        <p></p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <!-- Icon Box Type 2-->
                    <div class="unit unit-sm flex-md-row unit-spacing-sm text-left align-items-center">
                      <div class="unit-left"><span class="icon icon icon-sm icon-circle text-java icon-default  mdi mdi-star"></span></div>
                      <div class="unit-body">
                        <h5 class="font-weight-bold font-weight-bold text-primary">The Five Star Award</h5>
                        <p></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-5 col-xxl-4 section-90 section-md-122 context-md-dark bg-image-md-fullwidth-1 bg-image-md-fullwidth-1-left d-none d-xl-block"></div>
      
    </div>
  </div>
</section>