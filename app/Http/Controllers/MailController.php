<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Redirect;

class MailController extends Controller
{
    public function index(){
        return view('home');
    }
    public function enviarmail(Request $request)
    {
   
        $rules=[
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'last_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|email',
            'phone'=>'required|min:9|',
            'message'=>'required',
        ];

        $mensajes=[
            'name.required'=>'El nombre es obligatorio',
            'name.regex'=>'El nombre solo puede tener letras',
            'last_name.required'=>'El apellido es obligatorio',
            'last_name.regex'=>'El apellido solo puede tener letras',
            'email.required'=>'El correo electronico es obligatorio',
            'email.email'=>'El correo electronico debe tener un  formato valido',
            'message.required'=>'El mensaje es  obligatorio',
            'phone.required'=>'El telefono de contacto es  obligatorio',
            'phone.min'=>'El telefono debe tener minimo 9 digitos',
            'phone.regex'=>'no tiene un formato valido para telefono',
        ];
        $validator = \Validator::make($request->all(), $rules,$mensajes);
        if ($validator->fails()) {
            $notify['msg'] = 'Error al enviar formulario de contacto';
            $notify['type'] = 'danger';
            /*return Redirect::back()->withErrors($validator)->with(compact('notify'))->withInput($request->all());*/
            return response()->json(array('success'=>false,
                'errors'=>"Debe llenar los campos obligatorios",
                "x"=>$validator->errors()
            ),
            400);
        }
        try
        {

            $email="reus@clinicabares.com"; 
            $from= "formularioweb@clinicabares.com";
            $recibo="acampos@tecnobravo.com";
            $subject="Solicitud de informacion realizada desde el formulario de contactos Clínica Barés";
            $name=$request->name." ".$request->last_name;
            $phone=$request->phone;
            $msg=$request->message;
            $contacto=$request->email;
            $MAIL_USERNAME=env('MAIL_USERNAME');
            Mail::send('mail/contacto', 
            [
                'name' =>$name,
                'phone'=>$phone,
                'contacto'=>$email,
                'msg' =>$msg
            ], 
            function ($mail) use($email,$recibo,$from,$subject) {
                $mail->from($from, 'Clínica Barés');
                $mail->to($email);
                $mail->bcc($recibo);
                $mail->subject($subject);
            });
           /* Mail::send('mail/respuesta', 
            ['name' =>$name ], 
            function ($mail) use($contacto,$from,$subject) {
                $mail->from($from, 'Division de seguridad');
                $mail->to($contacto);
                $mail->bcc('comercial@tecnobravo.com');
                $mail->subject($subject);
            });*/
            return   response()->json([
                'success'=>true,
                'msg'=>"Se envio el correo con exito a Clínica Barés le estaremos contactando",
            ],200);
        }
        catch (Exception $e)
        {
            \Log::info('Error creando el Medio de contacto: '.$e);
            return \Response::json(['created' => false], 500);
        }
       
  }
}
